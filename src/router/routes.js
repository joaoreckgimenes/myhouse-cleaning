
const routes = [
  { path: '/', component: () => import('pages/Loading.vue') },
  { path: '/inicio', component: () => import('pages/Agenda.vue') },
  { path: '/login', component: () => import('pages/Login.vue') },
  { path: '/telaAprovacao', component: () => import('pages/telaAprovacao.vue') },
  // {
  //   path: '/',
  //   component: () => import('layouts/MainLayout.vue'),
  //   children: [
  //     { path: '', component: () => import('pages/Index.vue') }
  //   ]
  // },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '*',
    component: () => import('pages/Error404.vue')
  }
]

export default routes
