// This is just an example,
// so you can safely delete all default props below

export default {
  failed: 'Action failed',
  success: 'Action was successful',
  hello: 'helllllllllow',
  subtitleLogin: 'Lets get started now!',
  password: 'Password',
  signin: 'Sign In',
  createaccount: 'Create account',
  nohaveaccount: 'Dont have an account?',
  createone: 'Create One!',
  desconectar: 'Discconect',
  criarevento: 'Create Event',
  title: 'Title',
  datainicial: 'Date',
  datafinal: 'End Date',
  horainicial: 'Start Time',
  horafinal: 'End Time',
  deletarevento: 'Delete Event',
  nao: 'No',
  sim: 'Yes',
  salvar: 'Save',
  nome: 'Name',
  telefone: 'Phone',
  configlembrete: 'Configure Reminder',
  listaaprovacao: 'Approval List',
  titleTelaAprovacao: 'Your Registration request is in Approval',
  subtitleTelaAprovacao: 'Sent to the administrator!',
  buttonTelaAprovacao: 'Sign in with another account',
  configurarlembrete: 'Set up reminder',
  mensagempadraosms: 'Standard Message (SMS):',
  enviarsms: 'Send SMS',
  messageavisosms: '* When sending SMS, all events for the selected day will receive a Reminder SMS.',
  nenhumaaprovacaopendente: 'None Pending Approval',
  aprovar: 'To approve',
  negar: 'deny',
  subtitleTelaAprovacaoNegado: 'Your Registration request has been Denied by the Administrator!'
}
