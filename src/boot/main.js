var Firebase = require('firebase/app')

require('firebase/auth')
require('firebase/firestore')
var firebaseui = require('firebaseui')
var AWS = require('aws-sdk')

var carregarIdioma = (self) => {
  try {
    var l = window.navigator.language
    var idiom = l.toLowerCase()
    if (idiom.includes('pt')) {
      idiom = 'pt-br'
      return idiom
    }
    if (idiom.includes('es')) {
      idiom = 'es'
      return idiom
    }
    if (idiom.includes('en')) {
      idiom = 'en-us'
      return idiom
    }
    return idiom
  } catch (e1) {
    console.error(e1)
    return 'en-us'
  }
}

export default async ({ app, router, Vue }) => {
  try {
    Vue.prototype.$versao = '0.0.1'
    Vue.prototype.$bus = new Vue()
    var config = {
      apiKey: 'AIzaSyAKWety9bXvIlQym1Gn5xHzcwB9Jy5NkQQ',
      authDomain: 'myhousecleaning-company.firebaseapp.com',
      databaseURL: 'https://myhousecleaning-company.firebaseio.com',
      projectId: 'myhousecleaning-company',
      storageBucket: 'myhousecleaning-company.appspot.com',
      messagingSenderId: '57807924496',
      appId: '1:57807924496:web:d171660e7c163cad4bc995',
      measurementId: 'G-F3E0Q7SZTE'
    }
    Vue.prototype.$fbui = firebaseui
    Vue.prototype.$AWS = AWS
    // Initialize Firebase.
    var firebaseApp = Firebase.initializeApp(config)
    Vue.prototype.$fb = Firebase
    Vue.prototype.$fbApp = firebaseApp
    Vue.prototype.$auth = firebaseApp.auth()
    var firestore = firebaseApp.firestore()
    var idioma = carregarIdioma(Vue.prototype)
    Vue.prototype.$produto = {
      language: idioma
    }
    // var functions = firebaseApp.functions()
    // Vue.prototype.$func = functions
    firestore.enablePersistence({ synchronizeTabs: true })
      .then(function () {
        // Initialize Cloud Firestore through firebase
        Vue.prototype.$fs = firestore
        app.store.IsReady = true
        Vue.prototype.$bus.$emit('OnReady')
      })
      .catch(function (err) {
        Vue.prototype.$fs = firestore
        app.store.IsReady = true
        Vue.prototype.$bus.$emit('OnReady')
        if (err.code === 'failed-precondition') {
          if (alert('Você possui um ou mais abas do MyHouseCleaning abertas, fecha todas as guias e clique em OK para recarregar a página')) {
          } else {
            window.location.reload()
          }
          // Multiple tabs open, persistence can only be enabled
          // in one tab at a a time.
          // ...
        } else if (err.code === 'unimplemented') {
          // The current browser does not support all of the
          // features required to enable persistence
          // ...
        }
        // return Promise.reject(err)
      })
  } catch (e) {
    console.log('erro main.js: ' + e)
  }
}
